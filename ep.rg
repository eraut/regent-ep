-- Regent EP application

import "regent"

local c = regentlib.c

task count_stream(n_pairs: uint64, seed: uint, reg_count : region(ispace(int1d), uint64))
where
  writes(reg_count)
do
  var statebuf : int8[256]
  var buf : c.random_data
  buf.state = [&int32](0)
  c.initstate_r(seed+1, statebuf, 256, &buf)

  var count : uint64 = 0

  for i: uint64 = 0, n_pairs do

    var u1 : double, u2: double, z1: double, z2: double
    var rn: int32

    c.random_r(&buf, &rn)
    u1 = [double](rn)/([double](c.RAND_MAX)+1.0)
    c.random_r(&buf, &rn)
    u2 = [double](rn)/([double](c.RAND_MAX)+1.0)

    z1 = c.sqrt(-2.0*c.log(u1))*c.cos(2*c.M_PI*u2)
    z2 = c.sqrt(-2.0*c.log(u1))*c.sin(2*c.M_PI*u2)

    if c.fabs(z1) <= 1.0 then
      count += 1
    end
    if c.fabs(z2) <= 1.0 then
      count += 1
    end

  end

  for i in reg_count.ispace do
    reg_count[i] = count
  end

end

task sum_counts(n: uint64, counts: region(ispace(int1d), uint64))
where reads(counts)
do
  var sum : uint64 = 0
  for i in counts.ispace do
    sum += counts[i]
  end

  c.printf("Total: %lu / %lu (%g)\n", sum, n, [double](sum) / [double](n))
end

task toplevel()

  var n_streams : uint64 = 2
  var n_pairs_ps : uint64 = 8192

  -- Arguments
  var args = c.legion_runtime_get_input_args()
  for i = 0, args.argc do
    if c.strcmp(args.argv[i], "-s") == 0 then
      n_streams = c.atol(args.argv[i+1])
    elseif c.strcmp(args.argv[i], "-p") == 0 then
      n_pairs_ps = c.atol(args.argv[i+1])
    end
  end

  var is = ispace(int1d, n_streams)

  var counts = region(is, uint64)
  var part = partition(equal, counts, is)

  fill(counts, 0)

  __fence(__execution, __block)

  var t_start = c.legion_get_current_time_in_micros()

  for i in is do
    count_stream(n_pairs_ps, [uint](i), part[i])
  end

  sum_counts(n_streams*n_pairs_ps*2, counts)

  __fence(__execution, __block)

  var t_end = c.legion_get_current_time_in_micros()

  c.printf("Elapsed time: %g s\n", [double](t_end-t_start)/1.0e6);

end

regentlib.start(toplevel)
