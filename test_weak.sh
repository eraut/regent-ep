#!/bin/sh

regent=$HOME/Legion/legion/language/regent.py

set -o xtrace

export MV2_ENABLE_AFFINITY=0

for nthr in 1 2 4 8 16 32; do

  $regent ep.rg -ll:cpu $nthr -s $nthr -p $[2**25] 

done
