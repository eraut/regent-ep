#!/bin/sh

#SBATCH --nodes=8
#SBATCH --ntasks-per-node=1
#SBATCH --cpus-per-task=40
#SBATCH --time=04:00:00
#SBATCH -p short-40core

module load gcc/8.1.0
module load mpich

set -o xtrace

# Disable MV2 affinity
export MV2_ENABLE_AFFINITY=0

regent=$HOME/Legion/legion/language/regent.py
gasnetrun_ibv=$HOME/Legion/legion-gasnet/release/bin/gasnetrun_ibv

P=$[2**26]

# Baseline with one thread
time $regent ep.rg -ll:cpu 1 -s 1 -p $P

# 32 threads, one node
time $regent ep.rg -ll:cpu 32 -s 32 -p $P

# With GASNet
for n in 1 2 4 8; do
  time $gasnetrun_ibv -n $n -v $regent ep.rg -ll:cpu 32 -s $[32*n] -p $P
done
